[![build status](https://gitlab.com/ixilon/aws-aware-eureka-instance/badges/master/build.svg)](https://gitlab.com/ixilon/aws-aware-eureka-instance/commits/master)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/de.ixilon/aws-aware-eureka-instance/badge.svg)](https://maven-badges.herokuapp.com/maven-central/de.ixilon/aws-aware-eureka-instance)

#  aws-aware-eureka-instance #
Use Amazon AWS specific information for registration with Eureka.

## Usage ##
This is a Spring Boot module that contains auto-configuration code to add Amazon `DataCenterInfo` to `EurekaInstanceConfig`.
Simply add the JAR to the classpath.
