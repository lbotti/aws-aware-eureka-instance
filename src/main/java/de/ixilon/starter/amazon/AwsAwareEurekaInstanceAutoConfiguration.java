package de.ixilon.starter.amazon;

import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.context.annotation.Configuration;

import com.netflix.appinfo.AmazonInfo;

@Configuration
public class AwsAwareEurekaInstanceAutoConfiguration {

  public AwsAwareEurekaInstanceAutoConfiguration(EurekaInstanceConfigBean config) {
    AmazonInfo info = AmazonInfo.Builder.newBuilder().autoBuild("eureka");
    if (info.getId() != null) {
      config.setDataCenterInfo(info);
      info.getMetadata().put(
          AmazonInfo.MetaDataKey.publicHostname.getName(),
          info.get(AmazonInfo.MetaDataKey.publicIpv4));
      config.setHostname(info.get(AmazonInfo.MetaDataKey.publicHostname));
      config.setIpAddress(info.get(AmazonInfo.MetaDataKey.publicIpv4));
    }
  }

}
